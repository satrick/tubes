import React from 'react'; 
import { Layout, Menu, Card, Col, Row, Button, Icon, Typography } from 'antd';
import NavMenu from './Landing_Page/navbar/NavMenu';
import Konten from './Landing_Page/content/konten';
import './style.css';
import {BrowserRouter as Router,Switch,Route,Link} from "react-router-dom";

const { Header, Content, Footer } = Layout;
const { Title } = Typography;

function Landing() {
  return (
    <Layout className="layout">
    <Header  style={{position:'fixed',zIndex:5, width: '100%', borderBottom:'3px solid #1890FF' }}>
      <NavMenu />
    </Header>
    <Content style={{  margin:'43px 0px 0px 0px' }}>
    <Konten />
    </Content>
    <Footer style={{ textAlign: 'center' }}>Smart Bimbel  Created by Teknik Informatika UNRAM 2019/2020</Footer>
  </Layout>
 
  );
}


export default Landing;