import firebase from 'firebase'
import 'firebase/firestore'

const firebaseConfig = {
  apiKey: "AIzaSyDHf8Sd42MyW9IjQ8GgSUXcfrW2pPiFUhE",
  authDomain: "si-bimbel-72d80.firebaseapp.com",
  databaseURL: "https://si-bimbel-72d80.firebaseio.com",
  projectId: "si-bimbel-72d80",
  storageBucket: "si-bimbel-72d80.appspot.com",
  messagingSenderId: "741393674283",
  appId: "1:741393674283:web:97b10d7f68c335e430fa6e",
  measurementId: "G-1XYBX7NHW8"
};

const fconfig = firebase.initializeApp(firebaseConfig)

export default fconfig;