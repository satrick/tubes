import React from 'react'; 
import { Layout, Menu, Card, Col, Row, Button, Icon, Typography } from 'antd';
import '../../style.css';
import {BrowserRouter as Router,Switch,Route,Link} from "react-router-dom";
const { Title } = Typography;
const { Header, Content, Footer } = Layout;


function Status() {
  return (
  	<div>
    <Layout className="layout">       
    <Content >     
    </Content>
    <h1 style={{ textAlign: 'center',marginTop:'200px',color:'silver' }}>Silahkan lakukan pembayaran untuk dapat mengakses sistem</h1>
     <Card
      style={{ 
      	
      	
      	border:'none',
      	color:'white', 
      	background:'none', 
      	textAlign:'center', 
      	fontSize:'25px',
      	width:'100%',
      	zIndex:'8' 
      }}
      
    >
    <Button type="primary" shape="round" size="large" style={{marginTop:'5px',marginBottom:'15%', width:'200px',textAlign:'center'}} >
	          <Link to="/">Home</Link>
	</Button>
	</Card>
    <Footer style={{ textAlign: 'center', background:'#fff' }}>Smart Bimbel  Created by Teknik Informatika UNRAM 2019/2020</Footer>
  </Layout>
 </div>
  );
}


export default Status;