  import React,{useState} from 'react';
import bg2 from '../../img/bg2.jpg';
import {BrowserRouter as Router,Switch,Route,Link} from "react-router-dom";
import firebase from '../../koneksi_firebase';
import { useCollection } from 'react-firebase-hooks/firestore';
import { useAuthState } from 'react-firebase-hooks/auth';
import Swal from 'sweetalert2';

import {
  Form,
  Input,
  Tooltip,
  Icon,
  Cascader,
  Select,
  Row,
  Col,
  Checkbox,
  Button,
  AutoComplete,
} from 'antd';

const { Option } = Select;
const AutoCompleteOption = AutoComplete.Option;
  
 

    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 8 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
      },
    };



function Registrasi() {
  const [nama,setNama]=useState('')
  const [email,setEmail]=useState('')
  const [password,setPassword]=useState('')
  const level = "murid"
  const status = "tidak aktif" 



function onSubmit(e) {
        e.preventDefault()
        if (nama !== '' && email !== '' && password !== '') {
          const doc = firebase.firestore().collection('pengguna').doc()
          const hasil = doc.set({ nama, status, level, email, password, key:doc.id}).then(() => {
            setNama('')
            setEmail('')
            setPassword('')

          })
          firebase.auth().createUserWithEmailAndPassword(email, password).then(() => {
            setNama('')
            setEmail('')
            setPassword('')
          })

          Swal.fire({
            icon: 'success',
            title: 'Ok!',
            text: 'Akun berhasil didaftarkan!'
          })
          // console.log(nama);
          // console.log(email);
          // console.log(password);

        } 
      }


  return (
    <div style={{ background: '#f2f2f2', minHeight: 200 }}> 
    <div style={{ background: '#111',width:'100%', height:'100%', position:'fixed',zIndex:'1', opacity:'.8', }}></div>
    <img alt="example" src={bg2} style={{width:'100%', height:'100%',position:'fixed'}} />
    <article class=" mw6 center bg-white shadow-5 mt6 br-3 pa3 pa4-ns mv3 ba b--black-10 " style={{ borderRadius:'10px', position:'absolute', top:'10%', left:'10%', right:'10%', zIndex:'3'}} >
  
     <Form {...formItemLayout} style={{}} onSubmit={onSubmit} >
        <Form.Item label="Nama Lengkap">
          <Input 

            required
            name="nama"
            onChange={e=> setNama(e.currentTarget.value)}
            value={nama}
             />
        </Form.Item>
        <Form.Item label="Email">
          <Input 

            required
            type="email"
            name="nama"
            onChange={e=> setEmail(e.currentTarget.value)} 
            value={email}
            />
        </Form.Item>
        <Form.Item label="Password">
          <Input.Password

            required 
            name="nama"
            onChange={e=> setPassword(e.currentTarget.value)}
            value={password}
            />
        </Form.Item>

          <Button type="primary" htmlType="submit" style={{width:'100%'}}>
              Register
          </Button>
          <br/><br/>
          <Link to="/">
            <Button type="danger" className="cencel" style={{width:'100%'}}>
                Cancel
            </Button>
          </Link>
      </Form>
      Or <a href="/login"> Login now!</a>
      </article>
    </div>
  );
}




export default Registrasi;