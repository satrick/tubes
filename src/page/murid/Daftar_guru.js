
import React from 'react'; 
import { Layout, Menu, Breadcrumb,Upload, Icon, message } from 'antd';
import {BrowserRouter as Router,Switch,Route,Link} from "react-router-dom";
import '../../style.css';
import Header2 from '../header2.js';

const { Dragger } = Upload;
const { Header, Content, Footer, Sider } = Layout;
const { SubMenu } = Menu;

const props = {
  name: 'file',
  multiple: true,
  action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
  onChange(info) {
    const { status } = info.file;
    if (status !== 'uploading') {
      console.log(info.file, info.fileList);
    }
    if (status === 'done') {
      message.success(`${info.file.name} file uploaded successfully.`);
    } else if (status === 'error') {
      message.error(`${info.file.name} file upload failed.`);
    }
  },
};

class Daftar_guru extends React.Component {
  state = {
    collapsed: false,
  };

  onCollapse = collapsed => {
    console.log(collapsed);
    this.setState({ collapsed });
  };

render() {
    return (
      <Layout style={{ minHeight: '100vh' }}>
          <Sider collapsible collapsed={this.state.collapsed} onCollapse={this.onCollapse}>
         <span style={{color:'#f2f2f2', fontSize:'40px', textAlign:'center'}}><Icon type="read" style={{fontSize:'40px',width:'95%',textAlign:'center', marginTop:'20px'}}/></span>
           <Menu theme="dark" defaultSelectedKeys={['1']} mode="inline" >
            <Menu.Item key="1" style={{marginTop:'30px'}}>
              <Link to="/Daftar_Guru" >
              <Icon type="user" />
              <span >Daftar Guru</span>
              </Link>
            </Menu.Item>
            <Menu.Item key="logout" >
              <Link to="/" >
              <Icon type="logout" />
              <span >Keluar</span>
              </Link>
            </Menu.Item>           
          </Menu>

        </Sider>
        <Layout>
          <Header style={{ background: '#fff', padding: 0 }}>
          <Header2 />
          </Header>
          <Content style={{ margin: '0 16px' }}>
            <div>
            <Breadcrumb style={{ margin: '16px 0' }}>
             <h1>Daftar Guru</h1>
            </Breadcrumb>
              <div style={{ padding: 24, background: '#fff', minHeight: 360 }}>
              </div>
            </div>
          </Content>
          <Footer style={{ textAlign: 'center' }}>Ant Design ©2018 Created by Ant UED</Footer>
        </Layout>
      </Layout>
    );
  }
}
export default Daftar_guru;