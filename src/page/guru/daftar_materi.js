import React from 'react'; 
import { Layout, Menu, Breadcrumb,Upload, Icon, message, Table } from 'antd';
import {BrowserRouter as Router,Switch,Route,Link} from "react-router-dom";
import '../../style.css';
import Header2 from '../header2.js';



const { Header, Content, Footer, Sider } = Layout;
const { SubMenu } = Menu;

class Daftar_materi extends React.Component {
  state = {
    collapsed: false,
  };

  onCollapse = collapsed => {
    console.log(collapsed);
    this.setState({ collapsed });
  };

render() {
    return (
      <Layout style={{ minHeight: '100vh' }}>
          <Sider collapsible collapsed={this.state.collapsed} onCollapse={this.onCollapse}>
         <span style={{color:'#f2f2f2', fontSize:'40px', textAlign:'center'}}><Icon type="read" style={{fontSize:'40px',width:'95%',textAlign:'center', marginTop:'20px'}}/></span>
           <Menu theme="dark" defaultSelectedKeys={['data-murid']} mode="inline" >
         


            <Menu.Item key="data-murid" style={{marginTop:'30px'}}>
              <Link to="/Daftar-Materi">
              <Icon type="appstore" />
              <span>Daftar Materi</span>
              </Link>
            </Menu.Item>
           
            <Menu.Item key="upload">
              <Link to="/Upload-Materi" >
              <Icon type="upload" />
              <span>Upload Materi</span>
              </Link>
            </Menu.Item>

            <Menu.Item key="9">
            <Link to="/">
              <Icon type="logout" />
              <span>Keluar</span>
              </Link>
            </Menu.Item>
          </Menu>

        </Sider>
        <Layout>
          <Header style={{ background: '#fff', padding: 0 }}>
          <Header2 />
          </Header>
          <Content style={{ margin: '0 16px' }}>
            <Breadcrumb style={{ margin: '16px 0' }}>
             <h1>Daftar Materi</h1>
             
            </Breadcrumb>
          </Content>
          <Footer style={{ textAlign: 'center' }}>Smart Bimbel  Created by Teknik Informatika UNRAM 2019/2020</Footer>
        </Layout>
      </Layout>
    );
  }
}
export default Daftar_materi;