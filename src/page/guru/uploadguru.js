import React, { Component } from 'react'; 
import { Layout, Menu,Input, Breadcrumb,Upload, Icon,Button, message } from 'antd';
import {BrowserRouter as Router,Switch,Route,Link} from "react-router-dom";
import '../../style.css';
import Header2 from '../header2.js';
import axios from 'axios';


const { Dragger } = Upload;
const { Header, Content, Footer, Sider } = Layout;
const { SubMenu } = Menu;


class Uploadguru extends React.Component {
  state = {
    selectedFile: null
  }


  fileSelectedHandler = event => {
    this.setState({
      selectedFile: event.target.files[0]
    })
  }

  fileUploadHandler = event => {
    
  }

render() {
    return (
      <Layout style={{ minHeight: '100vh' }}>
          <Sider  onCollapse={this.onCollapse}>
         <span style={{color:'#f2f2f2', fontSize:'40px', textAlign:'center'}}><Icon type="read" style={{fontSize:'40px',width:'95%',textAlign:'center', marginTop:'20px'}}/></span>
           <Menu theme="dark" defaultSelectedKeys={['upload']} mode="inline" >
          
            <Menu.Item key="data-murid" style={{marginTop:'30px'}}>
            <Link to="/Daftar-Materi">
              <Icon type="appstore" />
              <span>Daftar Materi</span>
              </Link>
            </Menu.Item>

            <Menu.Item key="upload">
              <Link to="/Upload-Materi" >
              <Icon type="upload" />
              <span>Upload Materi</span>
              </Link>
            </Menu.Item>
        
           <Menu.Item key="9">
            <Link to="/">
              <Icon type="logout" />
              <span>Keluar</span>
              </Link>
            </Menu.Item>
          </Menu>

        </Sider>
        <Layout>
          <Header style={{ background: '#fff', padding: 0 }}>
          <Header2 />
          </Header>
          <Content style={{ margin: '0 16px' }}>
            <div>
            <Breadcrumb style={{ margin: '16px 0' }}>
             <h1>Upload Maeteri</h1>
            </Breadcrumb>
              <Input 
                type="file"
                accept="application/pdf"
                onChange={this.fileSelectedHandler}
              />
              <Button type="primary" htmlType="submit" style={{width:'100%'}} onClick={this.fileUploadHandler}>
                Upload
              </Button>
            </div>
          </Content>
          <Footer style={{ textAlign: 'center' }}>Smart Bimbel  Created by Teknik Informatika UNRAM 2019/2020</Footer>
        </Layout>
      </Layout>
    );
  }
}
export default Uploadguru;