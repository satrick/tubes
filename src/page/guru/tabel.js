import React from 'react'; 
import { Table, Button } from 'antd';

const columns = [
  
  {
    title: 'No.Siswa',
    dataIndex: 'no',
  },
  {
    title: 'Nama',
    dataIndex: 'name',
  },
  {
    title: 'Alamat',
    dataIndex: 'alamat',
  },
  {
    title: 'No.HP',
    dataIndex: 'no_hp',
  },
];

const data = [];
for (let i = 0; i < 46; i++) {
  data.push({
    key: i,
    no:`${i}`,
    name: `Edward King ${i}`,
    no_hp: 32,
    alamat: `London, Park Lane no. ${i}`,
  });
}

class Tabel extends React.Component {
  state = {
    selectedRowKeys: [], // Check here to configure the default column
    loading: false,
  };

  start = () => {
    this.setState({ loading: true });
    // ajax request after empty completing
    setTimeout(() => {
      this.setState({
        selectedRowKeys: [],
        loading: false,
      });
    }, 1000);
  };

  onSelectChange = selectedRowKeys => {
    console.log('selectedRowKeys changed: ', selectedRowKeys);
    this.setState({ selectedRowKeys });
  };

  render() {
    const { loading, selectedRowKeys } = this.state;
    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange,
    };
    const hasSelected = selectedRowKeys.length > 0;
    return (
      <div style={{background:'white'}}>
        <div style={{ marginBottom: 16 }}>
          
          <span style={{ marginLeft: 8 }}>
            {hasSelected ? `Selected ${selectedRowKeys.length} items` : ''}
          </span>
        </div>
        <Table rowSelection={rowSelection} columns={columns} dataSource={data} />
      </div>
    );
  }
}
export default Tabel;