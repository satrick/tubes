import React,{useState} from 'react';
import './style.css';
import {BrowserRouter as Router,Switch,Route,Link,Redirect} from "react-router-dom";
import bg2 from '../../img/bg2.jpg';
import firebase from '../../koneksi_firebase';
import { Form, Icon, Input, Button, Checkbox } from 'antd';
import { useAuthState } from 'react-firebase-hooks/auth';
import Swal from 'sweetalert2';
import { useCollection } from 'react-firebase-hooks/firestore';


function Login() {
  const [users,setUser] = useState('')
  const [pass,setPass] = useState('')
const [value, loading, errors] = useCollection(
    firebase.firestore().collection('pengguna'),
    {
      snapshotListenOptions: { includeMetadataChanges: true },
    }
  );



  const [user, initialising, error] = useAuthState(firebase.auth());
  const login = () => {
    firebase.auth().signInWithEmailAndPassword(users, pass);
  };
  function onSubmit(e) {
        e.preventDefault()
        if (users !== '' && pass !== '') {
          // const doc = firebase.firestore().collection('pengguna').doc()
          // const hasil = doc.set({ nama, status, level, email, password, key:doc.id}).then(() => {
          //   setNama('')
          //   setEmail('')
          //   setPassword('')

          // })
          firebase.auth().signInWithEmailAndPassword(users, pass).then(() => {
            // setNama('')
            setUser('')
            setPass('')
          })

          Swal.fire({
            icon: 'success',
            title: 'Ok!',
            text: 'Akun berhasil didaftarkan!'
          })
          // console.log(nama);
          // console.log(email);
          // console.log(password);

        } 
      }
  const logout = () => {
    firebase.auth().signOut();
  };
if (initialising) {
    return (
      <div>
        <p>Initialising User...</p>
      </div>
    );
  }
  if (user) {
const dummy=[]
if(dummy === []){

    return (
      <div>
        
      </div>
    );
  }else{
    console.log(dummy)
                return(
              <div>
              <p>
                {error && <strong>Error: {JSON.stringify(error)}</strong>}
                {loading && <span>Initialising: Loading...</span>}
                {value && (
                  <span>
                    Collection:{' '}
                    {value.docs.map(doc => (
                      <React.Fragment key={doc.id}>
                        {dummy.push(doc.data())},{' '}
                      </React.Fragment>
                    ))}
                  </span>
                )}
              </p>
              {dummy.filter(doc=> doc.email === user.email).map(doc=>(
              <div>
                <p>ini {doc.level}</p>
                {doc.level==='guru'?({doc.status==='tidak aktif'?(<div><Redirect to='/status'/></div>):(<div><Redirect to='/daftar-materi'/></div>)}),({doc.status==='tidak aktif'?(<div><Redirect to='/status'/></div>):(<div><Redirect to='/download-materi'/></div>)})}
                
                
                <p>Current User: {user.email}</p>
                <button onClick={logout}>Log out</button></div>))}      
              </div>
              )
    }
  }
  return (
    <div style={{ background: '#f2f2f2', minHeight: 200 }}> 
    <div style={{ background: '#111',width:'100%', height:'100%', position:'fixed',zIndex:'1', opacity:'.8', }}></div>
    <img alt="example" src={bg2} style={{width:'100%', height:'100%',position:'fixed'}} />
    <article class=" mw6 center bg-white shadow-5 mt6 br-3 pa3 pa4-ns mv3 ba b--black-10 " style={{ borderRadius:'10px', position:'absolute', top:'10%', left:'10%', right:'10%', zIndex:'3'}} >
     <Form style={{}} >
        <Form.Item>
          <Input 
            placeholder="Email"
            required
            type="email"
            name="email"
            onChange = {e=>setUser(e.currentTarget.value)}
            value={users}
            />
        </Form.Item>
        <Form.Item>
          <Input.Password
            placeholder="Password"
            required 
            name="pass"
            onChange = {e=>setPass(e.currentTarget.value)}
            value={pass}
            />
        </Form.Item>

          <Button type="primary" htmlType="submit" style={{width:'100%'}} onClick={login}>
              Login
          </Button>
          <br/><br/>
          <Link to="/">
            <Button type="danger" className="cencel" style={{width:'100%'}}>
                Cancel
            </Button>
          </Link>
      </Form>
      Or <a href="/registrasi"> Register now!</a>
      </article>
    </div>
  );

}

export default Login;