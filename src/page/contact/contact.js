import React from 'react'; 
import { Layout, Menu, Card, Col, Row, Button, Icon, Typography, Timeline, Tabs,Carousel } from 'antd';
import Konten from '../../Landing_Page/content/konten';
import '../../style.css';
import {BrowserRouter as Router,Switch,Route,Link} from "react-router-dom";
const { Title } = Typography;
const { Header, Content, Footer } = Layout;
const { Meta } = Card;

class Contact extends React.Component {
  state = {
    current: 'kontak',
  };

  handleClick = e => {
    console.log('click ', e);
    this.setState({
      current: e.key,
    });
  };


render() {
  return (
    <Layout className="layout">

        <Menu
        theme="light"
        mode="horizontal"
        defaultSelectedKeys={['kontak']}
       style={{ lineHeight: '60px',width:'100%',position:'fixed',zIndex:'17' }}
      >
        <Menu.Item key="judul" style={{ border:'none',fontWeight:'bold',fontSize:'15px', pointerEvent:'none', cursor:'default'}}>SMART <span style={{color:'#1890FF'}}>BIMBEL</span></Menu.Item>
        <Menu.Item key="home" style={{marginLeft:'20px'}}> <Link to="/"> Home </Link></Menu.Item>
        <Menu.Item key="kontak" ><Link to="/contact"> Contact </Link></Menu.Item>
        <Menu.Item key="about" > <Link to="/about"> About </Link></Menu.Item>

        <Menu.Item key="Login" ><Link to="/login"> Login </Link></Menu.Item>
        
      </Menu>
    <Content style={{  margin:'77px 0px 0px 0px' }}>
     <Col span={26}>
          <Card bordered={false} style={{color:'#1890FF', boxShadow:'5px 5px 50px grey'}}>
              <Timeline>
            <Timeline.Item dot={<Icon type="twitter" style={{ fontSize: '20px' }} />}>
              <span style={{marginLeft:'10px'}}>smartbimbel</span>  
            </Timeline.Item>
            <Timeline.Item dot={<Icon type="youtube" style={{ fontSize: '20px' }} />} >
              <span style={{marginLeft:'10px'}}>Smart Bimbel Oficial</span>  
            </Timeline.Item>
            <Timeline.Item dot={<Icon type="phone" style={{ fontSize: '20px' }} />} >
              <span style={{marginLeft:'10px'}}>222-233-221 atau 082247514552</span>  
            </Timeline.Item>
            <Timeline.Item  dot={<Icon type="google" style={{ fontSize: '20px'}} />} >
              <span style={{marginLeft:'10px'}}>smartbimbel@gmail.com</span>  
            </Timeline.Item>
        </Timeline>
          </Card>
        </Col>
    </Content>
    <Footer style={{ textAlign: 'center' }}>Smart Bimbel  Created by Teknik Informatika UNRAM 2019/2020</Footer>
  </Layout>
 
  );
}
}
export default Contact;