import React from 'react'; 
import { Layout, Menu, Breadcrumb, Icon,  Statistic, Row, Col, Button } from 'antd';
import {BrowserRouter as Router,Switch,Route,Link} from "react-router-dom";
import '../../style.css';
import foto from '../../img/bg3.jpg';

const { Header, Content, Footer, Sider } = Layout;
const { SubMenu } = Menu;
class Dashmurid extends React.Component {
  state = {
    collapsed: false,
  };

  onCollapse = collapsed => {

    console.log(collapsed);
    this.setState({ collapsed });
  };

render() {
    return (
      <Layout style={{ minHeight: '100vh' }}>
        <Sider collapsible collapsed={this.state.collapsed} onCollapse={this.onCollapse} >

<span style={{color:'#f2f2f2', fontSize:'40px', textAlign:'center'}}><Icon type="read" style={{fontSize:'40px',width:'95%',textAlign:'center', marginTop:'20px'}}/></span>
          <Menu theme="dark" defaultSelectedKeys={['1']} mode="inline" >
            <SubMenu
              key="tambah"
              title={
                <span>
                  <Icon type="user" />
                  <span>Daftar Guru</span>
                </span>
              }
            >
               <Menu.Item key="dataMurid"><Link to="/Data_Murid">jibar</Link></Menu.Item>
              <Menu.Item key="dataGuru"><Link to="/Data_Guru">jibar2</Link></Menu.Item>
            </SubMenu>
            
            <Menu.Item key="9">
            <Link to="/">
              <Icon type="logout" />
              <span>Keluar</span>
              </Link>
            </Menu.Item>
          </Menu>
        </Sider>
        <Layout>
          
          <Header style={{ background: '#fff', padding: 0 }}>
          
           <img alt="example" src={foto} style={{width:'45px', height:'45px', borderRadius:'50%', float:'right', margin:'10px',marginRight:'20px', border:'2px solid #333'}} /> 
          <h3 style={{float:'right'}}>Username</h3>

           </Header>
          <Content style={{ margin: '0 16px' }}>
            <Breadcrumb style={{ margin: '16px 0' }}>
            <h1>Dashboard Murid</h1>
              
            </Breadcrumb>
            <div style={{ padding: 24, background: '#fff', minHeight: 360 }}>
              <Row gutter={16} style={{marginBottom:'30px'}}>
                  <Col span={12}>

                    <Statistic title="JUMLAH MURID" value={112893} prefix={<Icon type="team" />} />
                  </Col>
                  <Col span={12}>
                    <Statistic title="JUMLAH MURID BARU" value={112893} prefix={<Icon type="user" />} />
                  </Col>
                </Row>
                <Row gutter={16}>
                  <Col span={12}>
                    <Statistic title="JUMLAH MATERI" value={112893} prefix={<Icon type="folder" />} />
                  </Col>
                  <Col span={12}>
                    <Statistic title="ONLINE" value={112893} precision={2} prefix={<Icon type="fire" />} />
                   
                  </Col>
                </Row>
            </div>
          </Content>
          <Footer style={{ textAlign: 'center' }}>Smart Bimbel  Created by Teknik Informatika UNRAM 2019/2020</Footer>
        </Layout>
      </Layout>
    );
  }
}
export default Dashmurid;