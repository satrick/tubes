import React from 'react'; 
import { Layout, Menu, Card, Col, Row, Button, Icon, Typography, Timeline, Tabs,Carousel } from 'antd';
import Konten from '../../Landing_Page/content/konten';
import '../../style.css';
import {BrowserRouter as Router,Switch,Route,Link} from "react-router-dom";
import Navbar from '../../Landing_Page/navbar/navbar';
const { Title } = Typography;
const { Header, Content, Footer } = Layout;

const { Meta } = Card;
class About extends React.Component {
  state = {
    current: 'about',
  };

  handleClick = e => {
    console.log('click ', e);
    this.setState({
      current: e.key,
    });
  };

render() {
  return (
    <Layout className="layout">
    <Menu
        theme="light"
        mode="horizontal"
        defaultSelectedKeys={['about']}
        style={{ lineHeight: '60px',width:'100%',position:'fixed',zIndex:'17' }}
      >
        <Menu.Item key="judul" style={{ border:'none',fontWeight:'bold',fontSize:'15px', pointerEvent:'none', cursor:'default'}}>SMART <span style={{color:'#1890FF'}}>BIMBEL</span></Menu.Item>
        <Menu.Item key="home" style={{marginLeft:'20px'}}> <Link to="/"> Home </Link></Menu.Item>
        <Menu.Item key="kontak" ><Link to="/contact"> Contact </Link></Menu.Item>
        <Menu.Item key="about" > <Link to="/about"> About </Link></Menu.Item>
        <Menu.Item key="Login" ><Link to="/login"> Login </Link></Menu.Item>
        
      </Menu>
    <Content style={{  margin:'77px 0px 39px 0px'  }}>
      <Col span={26}>
          <Card title="Tentang Smart Bimbel" bordered={false} style={{color:'#333', boxShadow:'5px 5px 50px grey'}}>
            Smart Bimbel merupakan kursus semua pelajaran yang dikhususkan untuk pelajar pelajar yang ingin lebih mendalami dan mempelajari dari suatu pelajaran tersebut. Smart Bimbel dibangun oleh mahasiswa informaika univeritas mataram pada tahun 2019 untuk dijadikan tugas besar mata kuliah Web Lanjut.  
          </Card>
        </Col>
     </Content>
    <Footer style={{ textAlign: 'center' }}>Smart Bimbel  Created by Teknik Informatika UNRAM 2019/2020</Footer>
  </Layout>
 
  );
}
}

export default About;