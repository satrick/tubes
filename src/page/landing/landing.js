import React from 'react'; 
import { Layout, Menu, Card, Col, Row, Button, Icon, Typography } from 'antd';
import Navbar from '../../Landing_Page/navbar/navbar';
import Konten from '../../Landing_Page/content/konten';
import '../../style.css';
import {BrowserRouter as Router,Switch,Route,Link} from "react-router-dom";
const { Title } = Typography;
const { Header, Content, Footer } = Layout;


function Landing() {
  return (
    <Layout className="layout">
       <Navbar />
    <Content >
     <Konten />
    </Content>
    <Footer style={{ textAlign: 'center', background:'#fff' }}>Smart Bimbel  Created by Teknik Informatika UNRAM 2019/2020</Footer>
  </Layout>
 
  );
}


export default Landing;