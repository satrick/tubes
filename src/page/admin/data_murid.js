import React from 'react'; 
import { Layout, Menu, Breadcrumb, Icon,Table, Dropdown, Button} from 'antd';
import {BrowserRouter as Router,Switch,Route,Link} from "react-router-dom";
import '../../style.css';
import Header2 from '../header2.js';
import firebase from '../../koneksi_firebase';
import { useCollection } from 'react-firebase-hooks/firestore';
import Swal from 'sweetalert2';


const { Header, Content, Footer, Sider } = Layout;
const { SubMenu } = Menu;

const Getmurid = () => {
  
  const [value, loading, error] = useCollection(
    firebase.firestore().collection('pengguna'),
    {
      snapshotListenOptions: { includeMetadataChanges: true },
    }
  );

  function delItem(isi) {
    Swal.fire({
      title: 'Apakah Anda Yakin?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Hapus!',
      cancelButtonText: 'Kembali'
    }).then((result) => {
      if (result.value) {
        Swal.fire({
          title: 'Terhapus',
          icon: 'success',
          confirmButtonColor: '#3085d6',
          confirmButtonText: 'Ok',
        }).then((result) => {
          if (result.value) {
            // window.location.reload()
          }
        })
     firebase.firestore().collection("pengguna").doc(isi).delete()
        // history.push('/categories/')
        // window.location.reload()
      }
    })
  }

  const getActionMenus = object => {
    return (
      <Menu>
        <Menu.Item key="1">
          <p onClick={e => delItem(object.key)}>Delete</p>
        </Menu.Item>
        <Menu.Item key="object-update">
          <Link to="/edit_data">Edit</Link>
        </Menu.Item>
      </Menu>
    );
  };

  const columns = [
    {
      title: 'Key',
      dataIndex: 'key',
      key: 'key',
    },
    {
      title: 'Nama',
      dataIndex: 'nama',
      key: 'nama',
    },
    {
      title: 'Email',
      dataIndex: 'email',
      key: 'email',
    },
    {
      title: 'Password',
      dataIndex: 'password',
      key: 'password',
    },
    {
      title: 'Level',
      dataIndex: 'level',
      key: 'level',
    },
    {
      title: 'Status',
      dataIndex: 'status',
      key: 'status',
    },
    {
      title: 'Action',
      key: 'action',
      render: (text, object) => (
        <Dropdown overlay={getActionMenus(object)} trigger={["click"]}>
          <Button shape="round" icon="more"></Button>
        </Dropdown>
      ),
    },
  ];

  const dummy=[]
    return (
      <div>
        <p style={{display:'none'}}>
          {error && <strong>Error: {JSON.stringify(error)}</strong>}
          {loading && <span>Collection: Loading...</span>}
          {value && (
            <span>
              Collection:{' '}
              {value.docs.map(doc => (
                <React.Fragment key={doc.id}>
                  {dummy.push(doc.data())},{' '}
                </React.Fragment>
              ))}
            </span>
          )}
        </p>
        <Table dataSource={dummy.filter(doc=>doc.level==='murid')} columns={columns} />
      </div>
    );
};


class Data_murid extends React.Component {
  state = {
    collapsed: false,
  };

  onCollapse = collapsed => {
    console.log(collapsed);
    this.setState({ collapsed });
  };

render() {
    return (
      <Layout style={{ minHeight: '100vh' }}>
          <Sider collapsible collapsed={this.state.collapsed} onCollapse={this.onCollapse}>
          
         <span style={{color:'#f2f2f2', fontSize:'20px', textAlign:'center'}}><Icon type="read" style={{fontSize:'40px',width:'95%',textAlign:'center', marginTop:'20px'}}/></span>
          <Menu theme="dark" defaultSelectedKeys={['dataMurid']} mode="inline" >
            <Menu.Item key="1" style={{marginTop:'30px'}}>
              <Link to="/Dashboard_Admin">
              <Icon type="dashboard" />
              <span >Dashboard</span>
              </Link>
            </Menu.Item>
            <SubMenu
              key="tambah"
              title={
                <span>
                  <Icon type="user" />
                  <span>Data User</span>
                </span>
              }
            >
               <Menu.Item key="dataMurid"><Link to="/Data_Murid">Murid</Link></Menu.Item>
              <Menu.Item key="dataGuru"><Link to="/Data_Guru">Guru</Link></Menu.Item>
            </SubMenu>
            <Menu.Item key="tambah">
              <Link to="/Tambah_user">
              <Icon type="plus" />
              <span >Tambah User</span>
              </Link>
            </Menu.Item>
            <Menu.Item key="9">
            <Link to="/">
              <Icon type="logout" />
              <span>Keluar</span>
            </Link>
            </Menu.Item>
          </Menu>
        </Sider>
        <Layout>
          <Header style={{ background: '#fff', padding: 0 }}>
          <Header2 />
          </Header>
          <Content style={{ margin: '0 16px' }}>
            <Breadcrumb style={{ margin: '16px 0' }}>
             <h1>Data Murid</h1>
            </Breadcrumb>
            <div style={{ padding: 24, background: '#fff', minHeight: 360 }}>

              < Getmurid/>

            </div>
          </Content>
          <Footer style={{ textAlign: 'center' }}>Smart Bimbel  Created by Teknik Informatika UNRAM 2019/2020</Footer>
        </Layout>
      </Layout>
    );
  }
}
export default Data_murid;