import React from 'react';
import { Layout, Menu, Breadcrumb, Icon,  Statistic, Row, Col, Button } from 'antd';
import {BrowserRouter as Router,Switch,Route,Link} from "react-router-dom";

const { SubMenu } = Menu;
class Menuu extends React.Component {
  state = {
    collapsed: false,
  };

  onCollapse = collapsed => {
    console.log(collapsed);
    this.setState({ collapsed });
  };

render() {
    return (
          <Menu theme="dark" defaultSelectedKeys={['1']} mode="inline">
            <Menu.Item key="1">
              <Icon type="dashboard" />
              <span><Link to="/Dashboard_Admin" style={{color:'white'}} >Dashboard</Link></span>
            </Menu.Item>
            <SubMenu
              key="tambah"
              title={
                <span>
                  <Icon type="user" />
                  <span>Data User</span>
                </span>
              }
            >
              <Menu.Item key="dataMurid"><Link to="/Data_Murid">Murid</Link></Menu.Item>
              <Menu.Item key="dataGuru"><Link to="/Data_Guru">Guru</Link></Menu.Item>
              
            </SubMenu>
            <SubMenu
              key="edit"
              title={
                <span>
                  <Icon type="plus" />
                  <span>Tambah User</span>
                </span>
              }
            >
              <Menu.Item key="addMurid"><Link to="/Tambah_Murid">Murid</Link></Menu.Item>
              <Menu.Item key="addGuru"><Link to="/Tambah_Guru">Guru</Link></Menu.Item>
            </SubMenu>
            <SubMenu
              key="setting"
              title={
                <span>
                  <Icon type="setting" />
                  <span>Pengaturan</span>
                </span>
              }
            >
              <Menu.Item key="Password"><Link to="/Data_Murid">Password</Link></Menu.Item>
              <Menu.Item key="user"><Link to="/Data_Guru">Data User</Link></Menu.Item>
              
            </SubMenu>

            <Menu.Item key="9">
              <Icon type="file" />
              <span>Keluar</span>
            </Menu.Item>
          </Menu>
            
      );
      }
    }

export default Menuu;