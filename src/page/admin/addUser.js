import React from 'react'; 
import { Layout, Menu, Breadcrumb, Icon,Input,Col } from 'antd';
import {BrowserRouter as Router,Switch,Route,Link} from "react-router-dom";
import '../../style.css';
import Header2 from '../header2.js';
import Inputuser from './input_user.js';

const { Header, Content, Footer, Sider } = Layout;
const { SubMenu } = Menu;
class AddUser extends React.Component {
  state = {
    collapsed: false,
  };

  onCollapse = collapsed => {
    console.log(collapsed);
    this.setState({ collapsed });
  };

render() {
    return (
      <Layout style={{ minHeight: '100vh' }}>
          <Sider collapsible collapsed={this.state.collapsed} onCollapse={this.onCollapse}>
        
         <span style={{color:'#f2f2f2', fontSize:'20px', textAlign:'center'}}><Icon type="read" style={{fontSize:'40px',width:'95%',textAlign:'center', marginTop:'20px'}}/></span>
          <Menu theme="dark" defaultSelectedKeys={['addMurid']} mode="inline" >
            <Menu.Item key="1" style={{marginTop:'30px'}}>
              <Link to="/Dashboard_Admin">
              <Icon type="dashboard" />
              <span >Dashboard</span>
              </Link>
            </Menu.Item>
            <SubMenu
              key="data"
              title={
                <span>
                  <Icon type="user" />
                  <span>Data User</span>
                </span>
              }
            >
               <Menu.Item key="dataMurid"><Link to="/Data_Murid">Murid</Link></Menu.Item>
              <Menu.Item key="dataGuru"><Link to="/Data_Guru">Guru</Link></Menu.Item>
            </SubMenu>
            
            <Menu.Item key="tambah">
              <Link to="/Tambah_user">
              <Icon type="plus" />
              <span >Tambah User</span>
              </Link>
            </Menu.Item>

            
            <Menu.Item key="9">
            <Link to="/">
              <Icon type="logout" />
              <span>Keluar</span>
            </Link>
            </Menu.Item>
          </Menu>
        </Sider>
        <Layout>
          <Header style={{ background: '#fff', padding: 0 }}>
          <Header2 />
          </Header>
          <Content style={{ margin: '0 16px' }}>
            <Breadcrumb style={{ margin: '16px 0' }}>
             <h1>Tambah User</h1>
            </Breadcrumb>
            <div style={{ padding: 24, background: '#fff', minHeight: 360 }}>
              <Inputuser />
            </div>
          </Content>
          <Footer style={{ textAlign: 'center' }}>Smart Bimbel  Created by Teknik Informatika UNRAM 2019/2020</Footer>
        </Layout>
      </Layout>
    );
  }
}


export default AddUser;