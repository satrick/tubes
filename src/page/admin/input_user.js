import React from 'react';
import bg2 from '../../img/bg2.jpg';
import {BrowserRouter as Router,Switch,Route,Link} from "react-router-dom";
import firebase from '../../koneksi_firebase';
import { useCollection } from 'react-firebase-hooks/firestore';
import { useAuthState } from 'react-firebase-hooks/auth';
import Swal from 'sweetalert2';
import {
  Form,
  Input,
  Tooltip,
  Icon,
  Cascader,
  Select,
  Row,
  Col,
  Checkbox,
  Button,
  AutoComplete,

} from 'antd';


const AutoCompleteOption = AutoComplete.Option;
const { Option } = Select;

const residences = [
  {
    value: 'zhejiang',
    label: 'Zhejiang',
    children: [
      {
        value: 'hangzhou',
        label: 'Hangzhou',
        children: [
          {
            value: 'xihu',
            label: 'West Lake',
          },
        ],
      },
    ],
  },
  {
    value: 'jiangsu',
    label: 'Jiangsu',
    children: [
      {
        value: 'nanjing',
        label: 'Nanjing',
        children: [
          {
            value: 'zhonghuamen',
            label: 'Zhong Hua Men',
          },
        ],
      },
    ],
  },
];




function Input_user () {
    return (
      <div >     
      <Form style={{}}>
        <Form.Item label="Nama Lengkap">
          <Input
            required
           />
        </Form.Item>
        <Form.Item label="Email">
          <Input
            required
            type="email"
           />
        </Form.Item>
         <Form.Item label="Status">
          
            <Select placeholder="Silahkan Masukkan Status">
              <Option value="on">Aktif</Option>
              <Option value="off">Belum Aktif</Option>
            </Select>,
          
        </Form.Item>
        <Form.Item label="Level">
            <Select placeholder="Silahkan Masukkan Level">
              <Option value="murid">Murid</Option>
              <Option value="guru">Guru</Option>
            </Select>
        </Form.Item>
        <Form.Item label="Password">
          <Input.Password />
        </Form.Item>      
        <Form.Item >
          <Button type="primary" htmlType="submit">
            Tambah
          </Button>
        </Form.Item>
      </Form>
      </div>
      
    );
}
export default Input_user;