import firebase from 'firebase';
import 'firebase/firestore';
import 'firebase/storage';



const firebaseConfig = {
    apiKey: "AIzaSyBTlII3T_rR8QgzKof51KgF_QxWBvfka8A",
    authDomain: "si-bimbel.firebaseapp.com",
    databaseURL: "https://si-bimbel.firebaseio.com",
    projectId: "si-bimbel",
    storageBucket: "si-bimbel.appspot.com",
    messagingSenderId: "428117536210",
    appId: "1:428117536210:web:c27a448753904f45ed2c0b",
    measurementId: "G-XDEH57YQQM"
  };

  const koneksi = firebase.initializeApp(firebaseConfig);

  export default koneksi;