
import React from 'react';
import { Layout, Menu, Breadcrumb } from 'antd';
import {BrowserRouter as Router,Switch,Route,Link} from "react-router-dom";
import { useAuthState } from 'react-firebase-hooks/auth';
import firebase from '../../koneksi_firebase';

const { Header, Content, Footer } = Layout;


function Navbar() {
  const [user, initialising, error] = useAuthState(firebase.auth());
  const logout = () => {
    firebase.auth().signOut();
  };
  function log(){
  if(user){
    return (
      
        // <Menu.Item>{user.email}</Menu.Item>
        <Menu.Item key="Logout" onClick={logout} style={{fontSize:'17px'}} >Logout</Menu.Item>
      
    )
  }else{
    return <Menu.Item key="Login" style={{fontSize:'17px'}} ><Link to="/login"> Login </Link></Menu.Item>
  }
  }
  
    return (  
      <div> 
      <Menu
        theme="light"
        mode="horizontal"
        defaultSelectedKeys={['home']}
        style={{ lineHeight: '60px',width:'100%',position:'fixed',zIndex:'17' }}
      >
        <Menu.Item key="judul" style={{ border:'none',fontWeight:'bold',fontSize:'19px', pointerEvent:'none', cursor:'default'}}>SMART <span style={{color:'#1890FF'}}>BIMBEL</span></Menu.Item>
        <Menu.Item key="home" style={{marginLeft:'20px',fontSize:'15px' }}> <Link to="/"> Home </Link></Menu.Item>
        <Menu.Item key="kontak" style={{fontSize:'15px'}} ><Link to="/contact"> Contact </Link></Menu.Item>
        <Menu.Item key="about" style={{fontSize:'15px'}}> <Link to="/about"> About </Link></Menu.Item>
        {log()}        
      </Menu>
      
      </div>
    );
  }


export default Navbar;
