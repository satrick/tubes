import React from 'react';
import { Layout, Menu, Card, Col, Row, Button, Icon, Typography } from 'antd';
import {BrowserRouter as Router,Switch,Route,Link} from "react-router-dom";
const { Title } = Typography;


function NavMenu() {
  return (
    <div>
      <div className="logo" >  
      </div>
      <Menu
        theme="dark"
        mode="horizontal"
        defaultSelectedKeys={['1']}
        style={{ lineHeight: '61px', borderBottom:'3px solid #1890FF', width:'100%'  }}>
        
        <Menu.Item key="1"> <Link to="/"> Home </Link></Menu.Item>
        <Menu.Item key="2"><Link to="/about"> About </Link></Menu.Item>
        <Menu.Item key="3"><Link to="/contact"> Contact us </Link></Menu.Item>
        <Menu.Item key="4"><Link to="/login"> Login </Link></Menu.Item>
      </Menu>
    </div>

);
}

export default NavMenu;