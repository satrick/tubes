import React from 'react';
import { Layout, Menu, Card, Col, Row, Button, Icon, Typography, Timeline, Tabs,Carousel } from 'antd';
import bg from '../../img/bg3.jpg';
import {BrowserRouter as Router,Switch,Route,Link} from "react-router-dom";
import './style.css';
const { Title } = Typography;
const { Meta } = Card;
function Konten() {
  return (
	 <div style={{ background: '#f2f2f2', minHeight: 200, marginTop:"60px" }}>	    
	
	     <div style={{ background: '#111',width:'100%', height:'500px', position:'absolute', opacity:'.6',zIndex:'4' }}>
		</div>
	     <img alt="example" src={bg} style={{width:'100%', height:'500px'}} />
    <Card
      style={{ 
      	position:'absolute',
      	top:'25%',
      	border:'none',
      	color:'white', 
      	background:'none', 
      	textAlign:'center', 
      	fontSize:'25px',
      	width:'100%',
      	zIndex:'8' 
      }}
      
    >
    <Icon type="read" style={{fontSize:'80px', marginBottom:'20px'}}/>
     <h4 style={{color:'white',textAlign:'center'}}>Belajar Lebih <span style={{color:'#1890FF', fontWeight:'bold', fontSize:'35px'}}>Mudah</span> dan <span style={{color:'#1890FF', fontWeight:'bold', fontSize:'35px'}}>Menyenangkan</span></h4> 
      <Button type="primary" shape="round" size="large" style={{marginTop:'30px', width:'200px'}} >
	          <Link to="/registrasi">Daftar Sekarang</Link>
	  </Button>
    </Card>
	

    <div style={{ background: '#ECECEC', padding: '30px',margin:'20px' }}>
	    <Row gutter={20} >
	      <Col span={26} >
	        <Card title="Waktu & Tempat Pendaftaran" bordered={false} style={{color:'#1890FF', boxShadow:'5px 5px 50px grey'}}>
	          <Timeline>
				    <Timeline.Item dot={<Icon type="clock-circle-o" style={{ fontSize: '16px' }} />} color="red">
				      Buka pada setiap hari senin s/d kamis, pukul 09.00 - 16.00 WITA 
				    </Timeline.Item>
				    <Timeline.Item dot={<Icon type="environment" style={{ fontSize: '16px' }} />}>JL. Majapahit No.04 Mataram NTB </Timeline.Item>
				  </Timeline>
	        </Card>
	      </Col>
	      <Col span={26}>
	        <Card title="Fasilitas & Kelengkapan" bordered={false} style={{color:'#1890FF', boxShadow:'5px 5px 50px grey'}}>
	          	<Timeline>
				    <Timeline.Item dot={<Icon type="read" style={{ fontSize: '16px' }} />}>
				      Buku dan Ebook gratis 
				    </Timeline.Item>
				    <Timeline.Item dot={<Icon type="laptop" style={{ fontSize: '16px' }} />} >
				      Free Komputer  
				    </Timeline.Item>
				    <Timeline.Item dot={<Icon type="global" style={{ fontSize: '16px' }} />} >
				      Free Wifi  
				    </Timeline.Item>
				    <Timeline.Item dot={<Icon type="video-camera" style={{ fontSize: '16px' }} />}>
				      Free Video Materi Pembelajaran  
				    </Timeline.Item>
				</Timeline>
	        </Card>
	      </Col>
	    </Row>
	  

	  </div>

     
	 </div>
	

	 

	);
}
export default Konten;