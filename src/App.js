import React from 'react'; 
import { Layout, Menu, Card, Col, Row, Button, Icon, Typography } from 'antd';
import Login from './page/Login/login';
// import Login_admin from './page/Login/loginAdmin';
import Registrasi from './page/registrasi/registrasi';
import Dashboard from './page/dashboard/dashboard';
import Dashadmin from './page/dashboard/dashadmin';
import Status from './page/statusNonaktif/status';
import Dashmurid from './page/dashboard/dashmurid';
import Landing from './page/landing/landing';
import About from './page/about/about';
import Contact from './page/contact/contact';
import AddUser from './page/admin/addUser';
import Edit from './page/admin/edit.js';
import Data_murid from './page/admin/data_murid';
import Data_guru from './page/admin/data_guru';
import Uploadguru from './page/guru/uploadguru';
import Daftar_materi from './page/guru/daftar_materi';
import Download_materi from './page/murid/download_materi';
import Daftar_guru from './page/murid/Daftar_guru';

import './style.css';
import {BrowserRouter as Router,Switch,Route,Link} from "react-router-dom";

const { Header, Content, Footer } = Layout;
const { Title } = Typography;

function App() {
  return (
    <div>
     <Switch>
     	<Route path="/" component={() =><div><Landing /></div>} exact />
     	<Route path="/about" component={() => <div><About /></div>} exact />
     	<Route path="/contact" component={() => <div><Contact /></div>} exact />
      <Route path="/login" component={() => <div><Login /></div>} exact />
     
      <Route path="/registrasi" component={() => <div><Registrasi /></div>} exact />
      <Route path="/Dashboard_Admin" component={() => <div><Dashadmin /></div>} exact />
      <Route path="/dashboard" component={() => <div><Dashboard /></div>} exact />
      <Route path="/Tambah_User" component={() => <div><AddUser /></div>} exact />
      <Route path="/Edit_data" component={() => <div><Edit /></div>} exact />      
      <Route path="/Data_Murid" component={() => <div><Data_murid /></div>} exact />
      <Route path="/Data_Guru" component={() => <div><Data_guru /></div>} exact />
      <Route path="/Status" component={() => <div><Status /></div>} exact />
      <Route path="/Upload-Materi" component={() => <div><Uploadguru /></div>} exact />
      <Route path="/Daftar-Materi" component={() => <div><Daftar_materi /></div>} exact />
      <Route path="/Dashboard_Murid" component={() => <div><Dashmurid /></div>} exact />
      <Route path="/Download-Materi" component={() => <div><Download_materi /></div>} exact />
      <Route path="/Daftar-Guru" component={() => <div><Daftar_guru /></div>} exact />

     </Switch>
 </div>
  );
}


export default App;